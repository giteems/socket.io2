package com.ms.mysocketdemo;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * @ProjectName: MySocketDemo
 * @Package: com.ms.mysocketdemo
 * @ClassName: SSLSocket
 * @Description: java类作用描述
 * @Author: 作者名
 * @CreateDate: 2020/4/30 8:08 PM
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/4/30 8:08 PM
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */
public class SSLSocket {
    public static SSLContext genSSLSocketFactory() {
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    X509Certificate[] x509Certificates = new X509Certificate[0];
                    return x509Certificates;
                }
            }}, new SecureRandom());
            return sc;
        } catch (Exception localException) {
//            LogHelper.e("SSLSocketFactory -> " + localException.getMessage());
        }
        return null;
    }
}

